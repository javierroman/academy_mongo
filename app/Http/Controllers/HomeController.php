<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Cups;
use Carbon\Carbon;
use Psy\Util\Json;

class HomeController extends Controller
{
    public function index()
    {

    }

    public function insert()
    {
        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->lecturas;

        $cupsGenerator = new Cups();

        $magnitudeList = ['active', 'reactive', 'max'];
        $typeList = ['signup', 'initial', 'real', 'billed'];

        //$collection->drop();

        for ($i = 0; $i < 1000000; $i++) {
            $cups = $cupsGenerator->generate();

            $counterId = rand(100000, 999999);

            for($j = 0; $j < 25; $j++) {
                $date = new Carbon();
                $randomDays = mt_rand(1,31);
                $date->addDays($randomDays);

                $randMagnitude = rand(0, 2);
                $randType = rand(0,3);

                $magnitude = $magnitudeList[$randMagnitude];
                $type = $typeList[$randType];

                for($k = 1; $k <= 3; $k++) {
                    $data = [
                        'cups' => $cups,
                        'datetime' => new \MongoDate(strtotime($date->toDateString())),
                        'clipsDate' => new \MongoDate(strtotime($date->toDateString())),
                        'counterId' => $counterId,
                        'period' => 'P' . $k,
                        'value' => rand(0, 10000),
                        'magnitude' => $magnitude,
                        'type' => $type
                    ];

                    $collection->insert($data);
                }
            }
            var_dump($cups);
        }
    }

    public function query()
    {
        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->lecturas;

//        $collection->ensureIndex(array("datetime" => -1));

        $start = new \MongoDate(strtotime("2016-03-01 00:00:00"));
        $end = new \MongoDate(strtotime("2016-03-06 00:00:00"));

        $select = $collection->find(array('datetime' => array('$gt' => $start, '$lte' => $end)));

        while($cups = $select->getNext()) {
            print_r('<pre>' . $cups['cups'] . ' ' . $cups['datetime']->toDateTime()->format('d/m/Y') . ' ' .
                $cups['period'] . ' ' . $cups['value'] .  '</pre>');
        }

//        var_dump($collection->find(array("lecturas.value" => array('$gt' => 9000))));

    }

    public function value()
    {
        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->lecturas;

        //$collection->ensureIndex(array("value" => 1));

        $select = $collection->find(array("value" => array('$gte' => 10000)));

        while($cups = $select->getNext()) {
            print_r('<pre>' . $cups['cups'] . ' ' . $cups['datetime']->toDateTime()->format('d/m/Y') . ' ' .
                $cups['period'] . ' ' . $cups['value'] .  '</pre>');
        }

    }

    public function cups($cups)
    {
        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->lecturas;

        //$collection->ensureIndex(array("cups" => 1));

        $select = $collection->find(array(
            'cups' => $cups
        ));

        while($cups = $select->getNext()) {
            print_r('<pre>' . $cups['cups'] . ' ' . $cups['datetime']->toDateTime()->format('d/m/Y') . ' ' .
                $cups['period'] . ' ' . $cups['value'] .  '</pre>');
        }

    }

}