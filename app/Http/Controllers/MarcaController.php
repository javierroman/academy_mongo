<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Cups;
use Carbon\Carbon;
use Psy\Util\Json;

class MarcaController extends Controller
{
    public function index()
    {
        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->marca;

        $select = $collection->find();

        while($comment = $select->getNext()) {
            echo "<h2>{$comment['title']}</h2>";
            echo "<p>{$comment['content']}</p>";
            echo "<h4>Comentarios:</h4>";
            echo "<ul>";
            foreach($comment['comments'] as $userComment) {
                echo "<li>{$userComment['comment']} - {$userComment['author']}</li>";
            }
            echo "</ul>";
        }
    }

    public function insert()
    {
        $faker = \Faker\Factory::create();

        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->marca;

        $collection->drop();

        for ($i = 0; $i < 10; $i++) {
            $comments = [];

            $comments[] = [
                'author' => 'Javi',
                'comment' => 'Poleeeeeeeeeeee'
            ];

            for($j = 0; $j < 99; $j++) {
                $author = rand(1, 500) > 1 ? $faker->name : 'Xavi';

                $comments[] = [
                    'author' => $author,
                    'comment' => $faker->sentence
                ];
            }

            $data = [
                'title' => $faker->sentence,
                'content' => $faker->paragraphs(3, true),
                'comments' => $comments

            ];

            $collection->insert($data);
        }
    }

    public function query($name)
    {
        $db = new \MongoClient("192.168.10.10:27017");
        $collection = $db->academy->marca;

        //$collection->ensureIndex(array("comments.author" => 1));

        $select = $collection->find(array('comments.author' => $name));

        while($comment = $select->getNext()) {
            echo "<h2>{$comment['title']}</h2>";
            echo "<p>{$comment['content']}</p>";
            echo "<h4>Comentarios:</h4>";
            echo "<ul>";
            foreach($comment['comments'] as $userComment) {
                if($userComment['author'] == $name) {
                    echo "<li>{$userComment['comment']} - {$userComment['author']}</li>";
                }
            }
            echo "</ul>";
        }
    }

}