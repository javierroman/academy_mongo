<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/insert', ['as' => 'insert', 'uses' => 'HomeController@insert']);
    Route::get('/query', ['as' => 'query', 'uses' => 'HomeController@query']);
    Route::get('/query-value/', ['as' => 'query-value', 'uses' => 'HomeController@value']);
    Route::get('/query-cups/{cups}', ['as' => 'query-cups', 'uses' => 'HomeController@cups']);

    Route::get('/marca', ['as' => 'marca-list', 'uses' => 'MarcaController@index']);
    Route::get('/marca/insert', ['as' => 'marca-insert', 'uses' => 'MarcaController@insert']);
    Route::get('/marca/buscar/{name}', ['as' => 'marca-buscar', 'uses' => 'MarcaController@query']);
});
